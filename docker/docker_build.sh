set -e
$(polly dockers login)

rm -rf temp
mkdir temp
cp -r ../tcga_rppa_connector/* temp/
cp ../requirements.txt temp/requirements.txt
ls -l temp/

docker build . -t docker.polly.elucidata.io/elucidata/connectors:tcga_rppa
rm -rf temp

docker push docker.polly.elucidata.io/elucidata/connectors:tcga_rppa
