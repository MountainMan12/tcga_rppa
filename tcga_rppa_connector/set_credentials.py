from discoverpy import Discover, download_helper, constants as cnst, env


def set_aws_credentials(repo_id):
    import os

    # Fetch sts tokens
    tokens = download_helper.get_tokens(repo_id)

    # set credentials in aws credentials file path
    os.system("mkdir ~/.aws")
    HOME_DIR = os.getenv("HOME")

    fp = open(HOME_DIR + "/.aws/credentials", "w")
    fp.write("[default]\n")
    fp.write(f'aws_access_key_id = {tokens["AccessKeyId"]}\n')
    fp.write(f'aws_secret_access_key = {tokens["SecretAccessKey"]}\n')
    if "SessionToken" in tokens:
        fp.write(f'aws_session_token = {tokens["SessionToken"]}')
    fp.close()
