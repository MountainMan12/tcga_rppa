    
convert_gct <- function(df, meta_df, output_filepath){
    require(mapGCT)
    # convert to numeric
    df[] <- sapply(df, as.numeric)

    # check set equality of colnames of matrix and row names of metadata
    if(!setequal(colnames(df), rownames(meta_df))){
        missing_samples <- c(setdiff(colnames(df), rownames(meta_df)),
                            setdiff(rownames(meta_df), colnames(df)))
        stop(paste0("samples ", paste(missing_samples, collapse = ", "),
                    "are not present in both matrix and metadata"))
    }

    # reorder metadata by sample names order of matrix
    meta_df <- meta_df[colnames(df), ]

    # make the gct and save to output path
    GCT_object <- to_GCT(mat=as.matrix(df), cdesc=meta_df)
    write_gct(GCT_object, output_filepath)
    return(output_filepath)
}
